/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __L_OBD_DTC_H__
#define __L_OBD_DTC_H__

#include "lobd.h"

/* $03. Show stored Diagnostic Trouble Codes */
void l_obd_stored_dtc (LOBDResponse *response);

/* $04. Clear Diagnostic Trouble Codes and stored values */
void l_obd_clear_dtc (LOBDResponse *response);

/* $07. Show pending Diagnostic Trouble Codes (detected during current or last driving cycle) */
void l_obd_pending_dtc (LOBDResponse *response);

/* $0A. Permanent DTC's (Cleared DTC's) */
void l_obd_permanent_dtc (LOBDResponse *response);

#endif /* __L_OBD_DTC_H__ */

