/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#include "lobd.h"

LOBDCallbackFunc _l_obd_callback_func = NULL;

void
l_obd_init (LOBDCallbackFunc func)
{
  _l_obd_callback_func = func;
}

void
l_obd_request_init (LOBDRequest *request,
		    uint8_t	 len,
		    uint8_t	 mode,
		    uint8_t	 pid)
{
  request->len	= len;
  request->mode	= mode;
  request->pid	= pid;
}

void
l_obd_response_receive (LOBDResponse *response)
{
  if (_l_obd_callback_func)
    _l_obd_callback_func (response);
}

