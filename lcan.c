/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

#include <libopencm3/cm3/systick.h>
#include <libopencm3/stm32/can.h>
#include <libopencm3/stm32/f1/flash.h>
#include <libopencm3/stm32/f1/gpio.h>
#include <libopencm3/stm32/f1/nvic.h>
#include <libopencm3/stm32/f1/rcc.h>

#include "lcan.h"

LCANCallbackFunc _l_can_callback_func = NULL;

void
l_can_init (LCANCallbackFunc func)
{
  _l_can_callback_func = func;

  /* Enable peripheral clocks. */
  rcc_peripheral_enable_clock (&RCC_APB2ENR, RCC_APB2ENR_AFIOEN);
  rcc_peripheral_enable_clock (&RCC_APB2ENR, RCC_APB2ENR_IOPBEN);
  rcc_peripheral_enable_clock (&RCC_APB1ENR, RCC_APB1ENR_CAN1EN);

  AFIO_MAPR |= AFIO_MAPR_CAN1_REMAP_PORTB;

  /* Configure CAN pin: RX (input pull-up). */
  gpio_set_mode (GPIO_BANK_CAN1_PB_RX, GPIO_MODE_INPUT,
                 GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_CAN1_PB_RX);
  gpio_set (GPIO_BANK_CAN1_PB_RX, GPIO_CAN1_PB_RX);

  /* Configure CAN pin: TX. */
  gpio_set_mode (GPIO_BANK_CAN1_PB_TX, GPIO_MODE_OUTPUT_50_MHZ,
                 GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_CAN1_PB_TX);

  /* NVIC setup. */
  nvic_enable_irq (NVIC_USB_LP_CAN_RX0_IRQ);
  nvic_set_priority (NVIC_USB_LP_CAN_RX0_IRQ, 1);

  /* Reset CAN. */
  can_reset (CAN1);

  if (can_init (CAN1,
                false,           /* TTCM: Time triggered comm mode? */
                true,            /* ABOM: Automatic bus-off management? */
                false,           /* AWUM: Automatic wakeup mode? */
                false,           /* NART: No automatic retransmission? */
                false,           /* RFLM: Receive FIFO locked mode? */
                false,           /* TXFP: Transmit FIFO priority? */
                CAN_BTR_SJW_1TQ,
                CAN_BTR_TS1_3TQ,
                CAN_BTR_TS2_4TQ,
                12,
                false,
                false))
  {
    /* Die because we failed to initialize. */
    while (1)
      __asm__ ("nop");
  }

  /* CAN filter 0 init. */
  can_filter_id_mask_32bit_init (CAN1,
                                 0,     /* Filter ID */
                                 0,     /* CAN ID */
                                 0,     /* CAN ID mask */
                                 0,     /* FIFO assignment (here: FIFO0) */
                                 true); /* Enable the filter. */

  /* Enable CAN RX interrupt. */
  can_enable_irq (CAN1, CAN_IER_FMPIE0);
}

void
l_can_request_init (LCANRequest	*request,
		    uint32_t	 id,
		    bool	 ext,
		    bool	 rtr,
		    uint8_t	 len,
		    uint8_t	*data)
{
/*uint32_t id = 0x7DF;
  bool     ext = false, rtr = true;
  uint8_t  len = 8; */

  request->id	= id;
  request->ext	= ext;
  request->rtr	= rtr;
  request->len	= len;

  len = len > 8 ? 8 : len;
  memcpy(request->data, data, len);
}

void
l_can_send (LCANRequest *request)
{
  can_transmit (CAN1,
                request->id,  /* (EX/ST)ID: CAN ID */
                request->ext, /* EXT: CAN ID extended? */
                request->rtr, /* RTR: Request transmit? */
                request->len, /* LEN: Data length */
                request->data);
}

void
usb_lp_can_rx0_isr (void)
{
  LCANResponse *response = malloc (sizeof (*response));

  can_receive(CAN1, 0, false, &response->id,
			      &response->ext,
			      &response->rtr,
			      &response->fmi,
			      &response->len,
			      response->data);
  can_fifo_release(CAN1, 0);

  if (_l_can_callback_func)
    _l_can_callback_func (response);
}

