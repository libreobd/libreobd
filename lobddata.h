/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __L_OBD_DATA_H__
#define __L_OBD_DATA_H__

#include "lobd.h"

/* $01. Show current data */
char* l_obd_current_data (LOBDResponse *response);

/* $02. Show freeze frame data */
char* l_obd_freeze_frame_data (LOBDResponse *response);

#endif /* __L_OBD_DATA_H__ */

