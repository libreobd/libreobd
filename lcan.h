/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __L_CAN_H__
#define __L_CAN_H__

#include <stdint.h>
#include <stdbool.h>

typedef struct _LCANRequest  LCANRequest;
typedef struct _LCANResponse LCANResponse;

typedef void (*LCANCallbackFunc) (LCANResponse *response);

struct _LCANRequest
{
  uint32_t id;
  bool     ext;
  bool     rtr;
  uint8_t  len;
  uint8_t  data[8];
};

struct _LCANResponse
{
  uint32_t id;
  bool     ext;
  bool     rtr;
  uint8_t  len;
  uint8_t  data[8];
  uint32_t fmi;
};

void l_can_init         (LCANCallbackFunc  func);
void l_can_request_init (LCANRequest      *request,
			 uint32_t         id,
			 bool             ext,
			 bool		  rtr,
			 uint8_t	  len,
			 uint8_t	 *data);
void l_can_request_send (LCANRequest      *request);

#endif /* __L_CAN_H__ */

