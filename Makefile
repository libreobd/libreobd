#
# This file is part of the LibreOBD project.
#
# Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#

CC	= arm-none-eabi-gcc

OBJCOPY	= arm-none-eabi-objcopy

CFLAGS	= -O0 -Wall -Wextra \
	  -I${HOME}/local/arm-none-eabi/include \
	  -fno-common -mthumb -mcpu=cortex-m3 -msoft-float -DSTM32F1

LDLIBS	= -lopencm3_stm32f1 -lc \
	  -L${HOME}/local/arm-none-eabi/lib \
	  -Tolimexino-stm32.ld -nostartfiles

all: libreobd.bin libreobd.hex

libreobd: lcan.o lobd.o lobdrouter.o lobddata.o lobddtc.o

%.bin: %
	$(OBJCOPY) -Obinary $(*) $(*).bin

%.hex: %
	$(OBJCOPY) -Oihex $(*) $(*).hex

clean:
	rm -f libreobd *.o *.d *.bin *.hex

