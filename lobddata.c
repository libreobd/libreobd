/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lobddata.h"

#include <stdio.h>

char* obd_calculated_engine_load_value      (LOBDResponse*);
char* obd_engine_coolant_temperature        (LOBDResponse*);
char* obd_fuel_percent_trim                 (LOBDResponse*);
char* obd_fuel_pressure                     (LOBDResponse*);
char* obd_intake_manifold_absolute_pressure (LOBDResponse*);
char* obd_engine_rpm                        (LOBDResponse*);
char* obd_vehicule_speed                    (LOBDResponse*);
char* obd_timing_advance                    (LOBDResponse*);
char* obd_intake_air_temperature            (LOBDResponse*);
char* obd_maf_air_flow_rate                 (LOBDResponse*);
char* obd_throttle_position                 (LOBDResponse*);
char* obd_oxygen_sensor_voltage             (LOBDResponse*);
char* obd_run_time_since_engine_start       (LOBDResponse*);
char* obd_distance_traveled_with_mil_on     (LOBDResponse*);

/* $01. Show current data */
char*
l_obd_current_data (LOBDResponse *response)
{
  switch (response->pid)
  {
    case 0x04:
      return obd_calculated_engine_load_value (response);

    case 0x05:
      return obd_engine_coolant_temperature (response);

    case 0x06:
    case 0x07:
    case 0x08:
    case 0x09:
      return obd_fuel_percent_trim (response);

    case 0X0A:
      return obd_fuel_pressure (response);

    case 0x0B:
      return obd_intake_manifold_absolute_pressure (response);

    case 0x0C:
      return obd_engine_rpm (response);

    case 0x0D:
      return obd_vehicule_speed (response);

    case 0x0E:
      return obd_timing_advance (response);

    case 0x0F:
      return obd_intake_air_temperature (response);

    case 0x10:
      return obd_maf_air_flow_rate (response);

    case 0x11:
      return obd_throttle_position (response);

    case 0x14:
    case 0x15:
    case 0x16:
    case 0x17:
    case 0x18:
    case 0x19:
    case 0x1A:
    case 0x1B:
      return obd_oxygen_sensor_voltage (response);

    case 0x1F:
      return obd_run_time_since_engine_start (response);

    case 0x21:
      return obd_distance_traveled_with_mil_on (response);
  }

  return NULL;
}

/* $02. Show freeze frame data */
char*
l_obd_freeze_frame_data (LOBDResponse *response)
{
  return l_obd_current_data (response);
}

/**
 * 01.00 PIDs supported [01 - 20]
 * 01.20 PIDs supported [21 - 40]
 * 01.40 PIDs supported [41 - 60]
 * 01.60 PIDs supported [61 - 80]
 * 01.80 PIDs supported [81 - A0]
 * 01.A0 PIDs supported [A1 - C0]
 * 01.C0 PIDs supported [C1 - E0]
 * 09.00 PIDs supported [01 - 20]
 */

/**
 * 01.01 Monitor status since DTCs cleared.
 * (Includes malfunction indicator lamp (MIL) status and number of DTCs.)
 */

/**
 * 01.02 Freeze DTC
 */

/**
 * 01.03 Fuel system status
 */

/**
 * 01.04 Calculated engine load value, 0, 100, %, A*100/255
 */
char*
obd_calculated_engine_load_value (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 6.2f %%", 100. * l_obd_response_value (response, uint8_t, 0) / 255);

  return string;
}

/**
 * 01.05 Engine coolant temperature, -40, 215, °C, A-40
 */
char*
obd_engine_coolant_temperature (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 3d °C", l_obd_response_value (response, uint8_t, 0) - 40);

  return string;
}

/**
 * 01.06 Short term fuel % trim - Bank 1, -100, 99.22, %, (A-128) * 100/128
 * 01.07 Long  term fuel % trim - Bank 1, -100, 99.22, %, (A-128) * 100/128
 * 01.08 Short term fuel % trim - Bank 2, -100, 99.22, %, (A-128) * 100/128
 * 01.09 Long  term fuel % trim - Bank 2, -100, 99.22, %, (A-128) * 100/128
 */
char*
obd_fuel_percent_trim (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 7.2f %%", 100. * (l_obd_response_value (response, uint8_t, 0) - 128) / 128);

  return string;
}

/**
 * 01.0A Fuel pressure, 0, 765, kPa (gauge), A*3
 */
char*
obd_fuel_pressure (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 3d kPa", 3 * l_obd_response_value (response, uint8_t, 0));

  return string;
}

/**
 * 01.0B Intake manifold absolute pressure, 0, 255, kPa (absolute), A
 */
char*
obd_intake_manifold_absolute_pressure (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 3d kPa", l_obd_response_value (response, uint8_t, 0));

  return string;
}

/**
 * 01.0C Engine RPM, 0, 16383.75, rpm, ((A*256)+B)/4
 */
char*
obd_engine_rpm (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 8.2f rpm", l_obd_response_value (response, uint16_t, 0) / 4.);

  return string;
}

/**
 * 01.0D Vehicule speed, 0, 255, km/h, A
 */
char*
obd_vehicule_speed (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 3d km/h", l_obd_response_value (response, uint8_t, 0));

  return string;
}

/**
 * 01.0E Timing advance, -64, 63.5, ° relative to #1 cylinder, A/2 - 64
 */
char*
obd_timing_advance (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 5.1f °", l_obd_response_value (response, uint8_t, 0) / 2. - 64);

  return string;
}

/**
 * 01.0F Intake air temperature, -40, 215, °C, A-40
 */
char*
obd_intake_air_temperature (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 3d °C", l_obd_response_value (response, uint8_t, 0) - 40);

  return string;
}

/**
 * 01.10 MAF air flow rate, 0, 655.35, grams/sec, ((A*256)+B) / 100
 */
char*
obd_maf_air_flow_rate (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 6.2f g/s", l_obd_response_value (response, uint16_t, 0) / 100.);

  return string;
}

/**
 * 01.11 Throttle position, 0, 100, %, A*100/255
 */
char*
obd_throttle_position (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 6.2f %%", 100. * l_obd_response_value (response, uint8_t, 0) / 255);

  return string;
}

/**
 * 01.12 Commanded secondary air status
 */

/**
 * 01.13 Oxygen sensors present
 */

/**
 * 01.14 Bank 1, Sensor 1: Oxygen sensor voltage, Short term fuel trim
 * 0 ; -100(lean), 1.275 ; 99.2(rich), Volts ; %
 * A/200 ; (B-128) * 100/128 (if B=0xFF, sensor is not used in trim calc)
 * 01.15 Bank 1, Sensor 2: Oxygen sensor voltage, Short term fuel trim
 * 01.16 Bank 1, Sensor 3: Oxygen sensor voltage, Short term fuel trim
 * 01.17 Bank 1, Sensor 4: Oxygen sensor voltage, Short term fuel trim
 * 01.18 Bank 2, Sensor 1: Oxygen sensor voltage, Short term fuel trim
 * 01.19 Bank 2, Sensor 2: Oxygen sensor voltage, Short term fuel trim
 * 01.1A Bank 2, Sensor 3: Oxygen sensor voltage, Short term fuel trim
 * 01.1B Bank 2, Sensor 4: Oxygen sensor voltage, Short term fuel trim
 */
char*
obd_oxygen_sensor_voltage (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string,
           "% 5.3f V, % 6.2f %%",
           l_obd_response_value (response, uint8_t, 0) / 200.,
           100. * (l_obd_response_value (response, uint8_t, 1) - 128) / 128);

  return string;
}

/**
 * 01.1C OBD standards this vehicle conforms to
 */

/**
 * 01.1D Oxygen sensors present
 */

/**
 * 01.1E Auxiliary input status
 */

/**
 * 01.1F Run time since engine start, 0, 65535, seconds, (A*256)+B
 */
char*
obd_run_time_since_engine_start (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 5d s", l_obd_response_value (response, uint16_t, 0));

  return string;
}

/**
 * 01.20 PIDs supported [21 - 40]
 *       @see 01.00
 */

/**
 * 01.21 Distance traveled with malfunction indicator lamp (MIL) on, 0, 65535,
 * km, (A*256)+B
 */
char*
obd_distance_traveled_with_mil_on (LOBDResponse *response)
{
  char*  string = NULL;

  asprintf(&string, "% 5d s", l_obd_response_value (response, uint16_t, 0));

  return string;
}

/**
 * 01.22 Fuel Rail Pressure (relative to manifold vacuum)
 */

/**
 * 01.23 Fuel Rail Pressure (diesel, or gasoline direct inject)
 */

/**
 * 01.24 O2S1_WR_lambda(1): Equivalence Ratio Voltage
 * 01.25 O2S2_WR_lambda(1): Equivalence Ratio Voltage
 * 01.26 O2S3_WR_lambda(1): Equivalence Ratio Voltage
 * 01.27 O2S4_WR_lambda(1): Equivalence Ratio Voltage
 * 01.28 O2S5_WR_lambda(1): Equivalence Ratio Voltage
 * 01.29 O2S6_WR_lambda(1): Equivalence Ratio Voltage
 * 01.2A O2S7_WR_lambda(1): Equivalence Ratio Voltage
 * 01.2B O2S8_WR_lambda(1): Equivalence Ratio Voltage
 */

/**
 * 01.2C Commanded EGR
 */

/**
 * 01.2D EGR Error
 */

/**
 * 01.2E Commanded evaporative purge
 */

/**
 * 01.2F Fuel Level Input
 */

/**
 * 01.30 # of warm-ups since codes cleared
 */

/**
 * 01.31 Distance traveled since codes cleared
 */

/**
 * 01.32 Evap. System Vapor Pressure
 */

/**
 * 01.33 Barometric pressure
 */

/**
 * 01.34 O2S1_WR_lambda(1): Equivalence Ratio Current
 * 01.35 O2S2_WR_lambda(1): Equivalence Ratio Current
 * 01.36 O2S3_WR_lambda(1): Equivalence Ratio Current
 * 01.37 O2S4_WR_lambda(1): Equivalence Ratio Current
 * 01.38 O2S5_WR_lambda(1): Equivalence Ratio Current
 * 01.39 O2S6_WR_lambda(1): Equivalence Ratio Current
 * 01.3A O2S7_WR_lambda(1): Equivalence Ratio Current
 * 01.3B O2S8_WR_lambda(1): Equivalence Ratio Current
 */

/**
 * 01.3C Catalyst Temperature Bank 1, Sensor 1
 * 01.3D Catalyst Temperature Bank 2, Sensor 1
 * 01.3E Catalyst Temperature Bank 1, Sensor 2
 * 01.3F Catalyst Temperature Bank 2, Sensor 2
 */

/**
 * 01.40 PIDs supported [41 - 60]
 *       @see 01.00
 */

/**
 * 01.41 Monitor status this drive cycle
 */

/**
 * 01.42 Control module voltage
 */

/**
 * 01.43 Absolute load value
 */

/**
 * 01.44 Command equivalence ratio
 */

/**
 * 01.45 Relative throttle position
 */

/**
 * 01.46 Ambient air temperature
 */

/**
 * 01.47 Absolute throttle position B
 * 01.48 Absolute throttle position C
 * 01.49 Accelerator pedal position D
 * 01.4A Accelerator pedal position E
 * 01.4B Accelerator pedal position F
 * 01.4C Commanded throttle actuator
 */

/**
 * 01.4D Time run with MIL on
 * 01.4E Time since trouble codes cleared
 */

/**
 * 01.4F Maximum value for equivalence ratio, oxygen sensor voltage, oxygen
 *       sensor current, and intake manifold absolute pressure
 */

/**
 * 01.50 Maximum value for air flow rate from mass air flow sensor
 */

/**
 * 01.51 Fuel Type
 */

/**
 * 01.52 Ethanol fuel %
 */

/**
 * 01.53 Absolute Evap system Vapor Pressure
 */

/**
 * 01.54 Evap system vapor pressure
 */

/**
 * 01.55 Short term secondary oxygen sensor trim bank 1 and bank 3
 * 01.56 Long  term secondary oxygen sensor trim bank 1 and bank 3
 * 01.57 Short term secondary oxygen sensor trim bank 2 and bank 4
 * 01.58 Long  term secondary oxygen sensor trim bank 2 and bank 4
 */

/**
 * 01.59 Fuel rail pressure (absolute)
 */

/**
 * 01.5A Relative accelerator pedal position
 */

/**
 * 01.5B Hybrid battery pack remaining life
 */

/**
 * 01.5C Engine oil temperature
 */

/**
 * 01.5D Fuel injection timing
 */

/**
 * 01.5E Engine fuel rate
 */

/**
 * 01.5F Emission requirements to which vehicle is designed
 */

/**
 * 01.60 PIDs supported [61 - 80]
 *       @see 01.00
 */

/**
 * 01.61 Driver's demand engine - percent torque
 * 01.62 Actual engine - percent torque
 */

/**
 * 01.63 Engine reference torque
 */

/**
 * 01.64 Engine percent torque data
 */

/**
 * 01.65 Auxiliary input / output supported
 */

/**
 * 01.66 Mass air flow sensor
 */

/**
 * 01.67 Engine coolant temperature
 */

/**
 * 01.68 Intake air temperature sensor
 */

/**
 * 01.69 Commanded EGR and EGR Error
 */

/**
 * 01.6A Commanded Diesel intake air flow control and relative intake air flow
 *       position
 */

/**
 * 01.6B Exhaust gas recirculation temperature
 */

/**
 * 01.6C Commanded throttle actuator control and relative throttle position
 */

/**
 * 01.6D Fuel pressure control system
 */

/**
 * 01.6E Injection pressure control system
 */

/**
 * 01.6F Turbocharger compressor inlet pressure
 */

/**
 * 01.70 Boost pressure control
 */

/**
 * 01.71 Variable Geometry turbo (VGT) control
 */

/**
 * 01.72 Wastegate control
 */

/**
 * 01.73 Exhaust pressure
 */

/**
 * 01.74 Turbocharger RPM
 */

/**
 * 01.75 Turbocharger temperature
 */

/**
 * 01.76 Turbocharger temperature
 */

/**
 * 01.77 Charge air cooler temperature (CACT)
 */

/**
 * 01.78 Exhaust Gas temperature (EGT) Bank 1
 * 01.79 Exhaust Gas temperature (EGT) Bank 2
 */

/**
 * 01.7A Diesel particulate filter (DPF)
 */

/**
 * 01.7B Diesel particulate filter (DPF)
 */

/**
 * 01.7C Diesel particulate filter (DPF) temperature
 */

/**
 * 01.7D NOx NTE control area status
 */

/**
 * 01.7E PM NTE control area status
 */

/**
 * 01.7F Engine run time
 */

/**
 * 01.80 PIDs supported [81 - A0]
 *       @see 01.00
 */

/**
 * 01.81 Engine run time for AECD
 * 01.82 Engine run time for AECD
 */

/**
 * 01.83 NOx sensor
 */

/**
 * 01.84 Manifold surface temperature
 */

/**
 * 01.85 NOx reagent system
 */

/**
 * 01.86 Particulate matter (PM) sensor
 */

/**
 * 01.87 Intake manifold absolute pressure
 */

/**
 * 01.A0 PIDs supported [A1 - C0]
 *       @see 01.00
 */

/**
 * 01.C0 PIDs supported [C1 - E0]
 *       @see 01.00
 */

/**
 * 01.C3 ?
 *       Returns numerous data, including Drive Condition ID and Engine Speed
 */

/**
 * 01.C4 ?
 *       B5 is Engine Idle Request
 *       B6 is Engine Stop Request
 */

/**
 * 02.02 Freeze frame trouble code
 */

