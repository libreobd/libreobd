/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>

#include "lcan.h"
#include "lobd.h"
#include "lobdrouter.h"

void obd_can_receive (LCANResponse *response)
{
  l_obd_response_receive ((LOBDResponse*) response->data);
}

int main ()
{
  l_obd_init (l_obd_router);
  l_can_init (obd_can_receive);

  return EXIT_SUCCESS;
}

