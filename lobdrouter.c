/*
 * This file is part of the LibreOBD project.
 *
 * Copyright (C) 2012 Gael Ducerf <galou@ozazar.org>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lobdrouter.h"

#include "lobddata.h"
#include "lobddtc.h"

void
l_obd_router (LOBDResponse *response)
{
  switch (response->mode)
  {
    /* $01. Show current data */
    case 0x01:
      l_obd_current_data (response);
      break;

    /* $02. Show freeze frame data */
      l_obd_freeze_frame_data (response);
    case 0x02:
      break;

    /* $03. Show stored Diagnostic Trouble Codes */
    case 0x03:
      l_obd_stored_dtc (response);
      break;

    /* $04. Clear Diagnostic Trouble Codes and stored values */
    case 0x04:
      l_obd_clear_dtc (response);
      break;

    /* $05. Test results, oxygen sensor monitoring (non CAN only) */
    case 0x05:

    /* $06. Test results, other component/system monitoring (Test results, oxygen sensor monitoring for CAN only) */
    case 0x06:
//      l_obd_test_results (response);
      break;

    /* $07. Show pending Diagnostic Trouble Codes (detected during current or last driving cycle) */
    case 0x07:
      l_obd_pending_dtc (response);
      break;

    /* $08. Control operation of on-board component/system */
    case 0x08:
//      l_obd_control_operation (response);
      break;

    /* $09. Request vehicle information */
    case 0x09:
//      l_obd_vehicule_information (response);
      break;

    /* $0A. Permanent DTC's (Cleared DTC's) */
    case 0x0A:
      l_obd_permanent_dtc (response);
      break;
  }
}

